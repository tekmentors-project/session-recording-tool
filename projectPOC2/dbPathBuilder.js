/* DBPathBuilder = function(){
    let BASE_PATH = 'https://session-recording-tool.firebaseio.com/';
    return {
        getWebsitePath = function(website){
            return `${BASE_PATH}/${website}.json`
        },
        getSessions = function(website){
            return `${BASE_PATH}/${website}.json?shallow=true`
        },
        getSessionDetail = function (website,sessionId){
            return `${BASE_PATH}/${website}/${sessionId}/sessionDetails.json`
        },
        getSessionData = function (website,sessionId){
            return `${BASE_PATH}/${website}/${sessionId}/sessionData.json`
        },
        saveSession = function(website){
            return `${BASE_PATH}/${website}.json`
        }
    }
}

export {DBPathBuilder} */

export class DBPathBuilder {
    constructor(path) {
        let BASE_PATH;
        if(!path){
            BASE_PATH = 'https://session-recording-tool.firebaseio.com/';
        }else{
            BASE_PATH = path;
        }
        
        this.getWebsitePath = function(website){
            return `${BASE_PATH}/${website}.json`;
        };
        this.getWebsiteDataByDate = function(website,date){

        }
        this.getSessions = function(website){
            return `${BASE_PATH}/${website}.json?shallow=true`
        };
        this.getSessionDetail = function (website,sessionId){
            return `${BASE_PATH}/${website}/${sessionId}/sessionDetails.json`
        };
        this.getSessionData = function (website,sessionId){
            return `${BASE_PATH}/${website}/${sessionId}/sessionData.json`
        };
        this.saveSession = function(website){
            return `${BASE_PATH}/${website}.json`
        }
    }
}