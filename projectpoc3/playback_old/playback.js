
(function(){

    let BASE_PATH = "https://irecord-1104f.firebaseio.com/website/";

    window.onload = function(){


        const websiteName = _getUrlParameter("websiteKeyName");

        _getObjects(websiteName);

    }

    function _getUrlParameter(name) {
        name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
        var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
        var results = regex.exec(location.search);
        return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
    };

    function _getObjects(websiteName) {

        if(!websiteName){
            websiteName='google_co_in'
        }
        //  https://irecord-1104f.firebaseio.com/website/google_co_in.json
        let testRunURL = BASE_PATH+websiteName+".json"

        return new Promise((resolve, reject) => {
            $.ajax({
                url: testRunURL,
                type: 'GET',
                contentType: 'text/plain',
                dataType: 'json',
                success: function(data) {
                    resolve(data);
                },
                error: function(error) {
                    console.log(error);
                    reject(error);
                }
            });
            
        });
    }
    
    
    _getObjects().then((websiteData) => {
        console.log('Promise is Resolved.');
        _handleSuccess(websiteData);
    }, (error) => {
        console.log('Promise is Rejected.');
        _handleError(error);
    });
    

    var _handleSuccess = function(websiteData){

        _createSessionCards(websiteData);

    }


    var _createSessionCards = function(websiteData){
        let datewiseSessions = _formatWebsiteData(websiteData);

        //let sessionsListelement = document.getElementById('');

        let sessionCardsElements = datewiseSessions.map((element,index)=>{

            console.log(element);

            let sessionCards = "";

            debugger;

            sessionCards = element.sessions.map((ele)=>{
                console.log(ele);

                if(ele.sessionDetails.baseUrl.indexOf('file') > -1) {
                    ele.sessionDetails.baseUrl = 'google.co.in';
                }


                /* return '<div class="card m-2 p-0 border border-info"><img class="card-img-top" src="./vedio.png" alt="Session Image" width=100 height=100><div class="card-body"><h5 class="card-title">'
                +ele.sessionDetails.date
                +'</h5><p class="card-text"> Website : ' 
                +ele.sessionDetails.baseUrl
                +'</p><a href="#" class="btn btn-success" style="border-radius: 10rem;">'
                +ele.sessionDetails.sessionLength
                +'</a></div></div>'; */

                return '<div class="row sessionRow" onClick="playSession(this)"><div class="col-2 text-center m-auto"><img class="img-thumbnail mx-auto" src="./vedio.png" alt="Session Image"></div><div class="col-10"><h5 class="card-title">'
                +ele.sessionDetails.date
                +'</h5><p class="card-text"> Website : '
                +ele.sessionDetails.baseUrl
                +'</p><a href="#" class="btn btn-success" style="border-radius: 10rem;">'
                +ele.sessionDetails.sessionLength
                +'</a></div></div><hr>';

            })


            return '<div class="card"><div class="card-header" id="heading"><h5 class="mb-0"><button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse_'
            +index
            +'" aria-expanded="true" aria-controls="collapseOne"><span>'
            +element.date
            +'</button></h5></div><div id="collapse_'
            +index
            +'" class="collapse" aria-labelledby="headingOne" data-parent="#sessionsListContainer"><div class="card-body p-1"><div id="sessionsList" class="col-12 p-0">'
            +sessionCards
            +'</div></div></div></div>';




        })

debugger;

        $('#sessionsListContainer').append(sessionCardsElements);
                     
        
       /*  $('#sessionsList').on('click', '.sessionRow', function(param1,param2){
            debugger;
        
            console.log(param1);
            console.log(param2);
        
        
         }) */
                    
    }

    var _formatWebsiteData = function(wesiteData){

        let entries = Object.entries(wesiteData)
        //console.log(entries)

        let datewiseSessions = entries.map((element)=>{
            let sessionDate = element[0];
            let sessionsList = Object.values(element[1]);
            return {
                date:sessionDate,
                sessions : sessionsList 
            }
        })

        return datewiseSessions;
    }


 var playSession = function(param1,param2){
            debugger;
        
            console.log(param1);
            console.log(param2);
        
        
         }
    




})()

