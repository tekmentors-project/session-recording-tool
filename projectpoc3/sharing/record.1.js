var jsReplay = (function () {

    // Indicates whether or not jsReplay is playing back user events. When set to true, jsReplay will not start another playback nor record user events.
    var playbackInProgress = false;

    // Indicates whether or not jsReplay is recording user events. When set to true, jsReplay will not start another recording nor start a playback.
    var recordInProgress = false;

    return {


        "record": (function () {

            var userEventLog = [];
            var ctrlKeyDown = false;

            // After recording is starting, startTimeDelay is set to the Unix time difference when the page was loaded and when recording started.
            // We use this value to adjust the timestamp stored on recorded events -- we don't want the dead time that occurs from when the page is loaded
            // until the recording is started to be reflected in our playback script.
            var startTimeDelay = new Date().getTime();

            /*	Function: _getSelectionText
					This function will retrieve the value of the text currently selected by the user.

				Returns: String
			*/
            var _getSelectionText = function () {
                var text = "";
                var activeEl = document.activeElement;
                var activeElTagName = activeEl ? activeEl.tagName.toLowerCase() : null;
                if (
                    (activeElTagName == "textarea") || (activeElTagName == "input" &&
                        /^(?:text|search|password|tel|url)$/i.test(activeEl.type)) &&
                    (typeof activeEl.selectionStart == "number")
                ) {
                    text = activeEl.value.slice(activeEl.selectionStart, activeEl.selectionEnd);
                } else if (window.getSelection) {
                    text = window.getSelection().toString();
                }
                return text;
            };


            var _getDeviceDetails = function () {
                'use strict';

                var module = {
                    options: [],
                    header: [navigator.platform, navigator.userAgent, navigator.appVersion, navigator.vendor, window.opera],
                    dataos: [{
                        name: 'Windows Phone',
                        value: 'Windows Phone',
                        version: 'OS'
                    },
                    {
                        name: 'Windows',
                        value: 'Win',
                        version: 'NT'
                    },
                    {
                        name: 'iPhone',
                        value: 'iPhone',
                        version: 'OS'
                    },
                    {
                        name: 'iPad',
                        value: 'iPad',
                        version: 'OS'
                    },
                    {
                        name: 'Kindle',
                        value: 'Silk',
                        version: 'Silk'
                    },
                    {
                        name: 'Android',
                        value: 'Android',
                        version: 'Android'
                    },
                    {
                        name: 'PlayBook',
                        value: 'PlayBook',
                        version: 'OS'
                    },
                    {
                        name: 'BlackBerry',
                        value: 'BlackBerry',
                        version: '/'
                    },
                    {
                        name: 'Macintosh',
                        value: 'Mac',
                        version: 'OS X'
                    },
                    {
                        name: 'Linux',
                        value: 'Linux',
                        version: 'rv'
                    },
                    {
                        name: 'Palm',
                        value: 'Palm',
                        version: 'PalmOS'
                    }
                    ],
                    databrowser: [{
                        name: 'Chrome',
                        value: 'Chrome',
                        version: 'Chrome'
                    },
                    {
                        name: 'Firefox',
                        value: 'Firefox',
                        version: 'Firefox'
                    },
                    {
                        name: 'Safari',
                        value: 'Safari',
                        version: 'Version'
                    },
                    {
                        name: 'Internet Explorer',
                        value: 'MSIE',
                        version: 'MSIE'
                    },
                    {
                        name: 'Opera',
                        value: 'Opera',
                        version: 'Opera'
                    },
                    {
                        name: 'BlackBerry',
                        value: 'CLDC',
                        version: 'CLDC'
                    },
                    {
                        name: 'Mozilla',
                        value: 'Mozilla',
                        version: 'Mozilla'
                    }
                    ],
                    init: function () {
                        var agent = this.header.join(' '),
                            os = this.matchItem(agent, this.dataos),
                            browser = this.matchItem(agent, this.databrowser);

                        return {
                            os: os,
                            browser: browser
                        };
                    },
                    matchItem: function (string, data) {
                        var i = 0,
                            j = 0,
                            html = '',
                            regex,
                            regexv,
                            match,
                            matches,
                            version;

                        for (i = 0; i < data.length; i += 1) {
                            regex = new RegExp(data[i].value, 'i');
                            match = regex.test(string);
                            if (match) {
                                regexv = new RegExp(data[i].version + '[- /:;]([\\d._]+)', 'i');
                                matches = string.match(regexv);
                                version = '';
                                if (matches) {
                                    if (matches[1]) {
                                        matches = matches[1];
                                    }
                                }
                                if (matches) {
                                    matches = matches.split(/[._]+/);
                                    for (j = 0; j < matches.length; j += 1) {
                                        if (j === 0) {
                                            version += matches[j] + '.';
                                        } else {
                                            version += matches[j];
                                        }
                                    }
                                } else {
                                    version = '0';
                                }
                                return {
                                    name: data[i].name,
                                    version: parseFloat(version)
                                };
                            }
                        }
                        return {
                            name: 'unknown',
                            version: 0
                        };
                    }
                };

                var e = module.init(),
                    debug = '';

                debug.osName = e.os.name;
                debug.osVersion = e.os.version;
                debug.browserName = e.browser.name;
                debug.browserVersion = e.browser.version;

                debug.userAgent = navigator.userAgent;
                debug.appVersion = navigator.appVersion;
                debug.platform = navigator.platform;
                debug.vendor = navigator.vendor;

                return debug;
            };

            /*	Function: logEvent
					This function will parse the 

			*/
            var logEvent = function (event) {
                // Only record the event if recording is in progress
                if (recordInProgress == true) {

                    var userEvent = {
                        "selector": getSelector(event.target)
                    };

                    if (event.type === "scroll") {
                        userEvent.type = "scroll";
                        userEvent.scrollTop = $(event.target).scrollTop();
                        userEvent.scrollLeft = $(event.target).scrollLeft();
                        userEvent.timeStamp = event.timeStamp;
                    } else {
                        for (var prop in event) {
                            // We can only record plain such as string, numbers and booleans in JSON. Objects will require special processing.
                            if (["number", "string", "boolean"].indexOf(typeof event[prop]) > -1
                                // Exclude certain event event attributes in order to keep the JSON log as small as possible.
                                // These attributes are not needed to re-create the event during playback.
                                &&
                                ["AT_TARGET", "BUBBLING_PHASE", "CAPTURING_PHASE", "NONE", "DOM_KEY_LOCATION_STANDARD", "DOM_KEY_LOCATION_LEFT", "DOM_KEY_LOCATION_RIGHT", "DOM_KEY_LOCATION_NUMPAD"].indexOf(prop) == -1) {
                                userEvent[prop] = event[prop];
                            } else if (["touches", "changedTouches"].indexOf(prop) > -1) {

                                userEvent[prop] = [];

                                for (var i = 0; i < event[prop].length; i++) {
                                    var touch = event[prop][i];
                                    userEvent[prop].push({
                                        "clientX": touch.clientX,
                                        "clientY": touch.clientY,
                                        "force": touch.force,
                                        "identifier": touch.identifier,
                                        "pageX": touch.pageX,
                                        "pageY": touch.pageY,
                                        "radiusX": touch.radiusX,
                                        "radiusY": touch.radiusY,
                                        "rotationAngle": touch.rotationAngle,
                                        "screenX": touch.screenX,
                                        "screenY": touch.screenY,
                                        "selector": getSelector(touch.target)
                                    });

                                }

                            }
                        }
                    }

                    // Subtract the start time delay from the timestamp so we don't include the dead time (i.e., time between
                    // page load and recording started) in our playback JSON log.
                    userEvent.timeStamp = userEvent.timeStamp - startTimeDelay;

                    if (userEvent.selector !== null) {
                        if (playbackInProgress == false) {
                            userEventLog.push(userEvent);
                            console.log("Logged " + userEvent.type + " event.");
                        }
                    } else {
                        console.warn("Null selector");
                    }
                }
            };

            /*	Function: getSelector
					This function starts at the DOM element specified by 'el' and traverses upward through the DOM tree building out a unique 
					CSS selector for the DOM element 'el'.

				Parameters:
					el - DOM element, the element that we want to determine CSS selector
					names - Array of strings, records the CSS selectors for the target element and parent elements as we progress up the DOM tree.

				Returns:
					String, a unique CSS selector for the target element (el).
			*/
            var getSelector = function (el, names) {
                if (el === document || el === document.documentElement) return "document";
                if (el === document.body) return "body";
                if (typeof names === "undefined") var names = [];
                if (el.id) {
                    names.unshift('#' + el.id);
                    return names.join(" > ");
                } else if (el.className) {
                    var arrNode = [].slice.call(el.parentNode.getElementsByClassName(el.className));
                    var classSelector = el.className.split(" ").join(".");
                    if (arrNode.length == 1) {
                        names.unshift(el.tagName.toLowerCase() + "." + classSelector);
                    } else {
                        for (var c = 1, e = el; e.previousElementSibling; e = e.previousElementSibling, c++);
                        names.unshift(el.tagName.toLowerCase() + ":nth-child(" + c + ")");
                    }
                } else {
                    for (var c = 1, e = el; e.previousElementSibling; e = e.previousElementSibling, c++);
                    names.unshift(el.tagName.toLowerCase() + ":nth-child(" + c + ")");
                }

                if (el.parentNode !== document.body) {
                    getSelector(el.parentNode, names)
                }
                return names.join(" > ");
            };

            document.addEventListener('click', function (event) {
                logEvent(event);
            }, true);
            document.addEventListener('mousedown', function (event) {
                logEvent(event);
            }, true);
            document.addEventListener('mouseup', function (event) {

                logEvent(event);

                // if the user has selected text, then we want to record an extra 'contains' event. on playback, this is used
                // to verify that the selected text is contained within the target element
                var selectedText = _getSelectionText();
                if (selectedText.length > 1) {
                    logEvent({
                        "target": document.activeElement,
                        "type": "contains",
                        "text": selectedText,
                        "timeStamp": event.timeStamp
                    });
                }
            }, true);
            document.addEventListener('input', function (event) {
                logEvent($.extend(true, event, {
                    "value": $(event.target).val()
                }));
            }, true);
            document.addEventListener('focus', function (event) {
                logEvent(event);
            }, true);
            document.addEventListener('focusin', function (event) {
                logEvent(event);
            }, true);
            document.addEventListener('focusout', function (event) {
                logEvent(event);
            }, true);
            document.addEventListener('blur', function (event) {
                logEvent(event);
            }, true);
            document.addEventListener('keypress', function (event) {
                logEvent(event);
            }, true);
            document.addEventListener('keydown', function (event) {
                logEvent(event);
            }, true);
            document.addEventListener('keyup', function (event) {
                logEvent(event);
            }, true);
            document.addEventListener('touchstart', function (event) {
                logEvent(event);
            }, true);
            document.addEventListener('touchend', function (event) {
                logEvent(event);
            }, true);
            document.addEventListener('touchmove', function (event) {
                logEvent(event);
            }, true);
            document.addEventListener('touchcancel', function (event) {
                logEvent(event);
            }, true);
            document.addEventListener('scroll', function (event) {
                logEvent(event);
            }, true);

            var _getsessionDetails = function (playbackData) {
                let baseUrl = window.location.href;
                let date = _getCurrentDate().split('_').join(':');
                let sessionLength;

                let loglength = playbackData.event_log.length - 1;

                sessionLength = playbackData.event_log[loglength].timeStamp - playbackData.event_log[0].timeStamp;

                sessionLength = _getSessionDuration(sessionLength / 1000);

                return {
                    'baseUrl': baseUrl,
                    'date': date,
                    'sessionLength': sessionLength
                }
            }

            var _getSessionDuration = function (timestamp) {
                var sec_num = parseInt(timestamp, 10);
                var hours = Math.floor(sec_num / 3600);
                var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
                var seconds = sec_num - (hours * 3600) - (minutes * 60);

                if (hours < 10) { hours = "0" + hours; }
                if (minutes < 10) { minutes = "0" + minutes; }
                if (seconds < 10) { seconds = "0" + seconds; }
                return hours + ':' + minutes + ':' + seconds;
            }

            var _getCurrentDate = function () {
                var today = new Date();
                var dd = today.getDate();
                var mm = today.getMonth() + 1; //January is 0!
                var yyyy = today.getFullYear();

                if (dd < 10) {
                    dd = '0' + dd
                }

                if (mm < 10) {
                    mm = '0' + mm
                }

                today = dd + '_' + mm + '_' + yyyy;

                return today;
            }

            var _saveSessionDetailstoDB = function (playbackData) {

                let BASE_PATH = 'https://irecord-1104f.firebaseio.com/';

                return Promise((resolve,reject)=>{

                    websiteName = window.location.host;
                    if (websiteName.indexOf('file') > -1) {
                        websiteName = 'google.co.in';
                    }
                    websiteName = websiteName.replace(/[\. /,:-]+/g, "_");
    
                    let saveSessionDetailsUrl = BASE_PATH + 'website/' + websiteName + '/' + _getCurrentDate() + '.json';
    
                    /* let saveSessionDataUrl = BASE_PATH + 'websiteData/' + websiteName + '/' + _getCurrentDate() + '.json';
                    let sessionData = playbackData;
                    let sessionData = {
                        'timestamp': sessionTimestamp,
                        'sessionData': sessionData
                    } */
                    let sessionDetails = _getsessionDetails(playbackData);
    
                    
                    let sessionTimestamp = new Date().getTime();
                    let sessionDetailsData = {
                        'timestamp': sessionTimestamp,
                        'sessionDetails': sessionDetails
                    }
    
                    $.ajax({
                        url: saveSessionDetailsUrl,
                        type: 'POST',
                        data: JSON.stringify(sessionDetailsData),
                        contentType: 'text/plain',
                        dataType: 'json',
                        async: false,
                        success: function (data) {
                            console.log("Event success.....");
                            debugger;
                            resolve(data);
                        },
                        error: function (error) {
                            console.log(error);
                            console.log("Event failed.....");
                            reject(error);
                        }
                    });

                })

            }

            
            var _saveSessionDatatoDB = function (playbackData , uniqueSessionId) {

                let BASE_PATH = 'https://irecord-1104f.firebaseio.com/';

                console.log(uniqueSessionId)
                debugger;

                return Promise((resolve,reject)=>{

                    websiteName = window.location.host;
                    if (websiteName.indexOf('file') > -1) {
                        websiteName = 'google.co.in';
                    }
                    websiteName = websiteName.replace(/[\. /,:-]+/g, "_");
    
                    //let saveSessionDetailsUrl = BASE_PATH + 'website/' + websiteName + '/' + _getCurrentDate() + '.json';
    
                    let saveSessionDataUrl = BASE_PATH + 'websiteData/' + websiteName + '/' + _getCurrentDate() + '/' + uniqueSessionId+'.json';
                    let sessionTimestamp = new Date().getTime();
                    let sessionData = {
                        'timestamp': sessionTimestamp,
                        'sessionData': playbackData
                    }

    
                    $.ajax({
                        url: saveSessionDataUrl,
                        type: 'POST',
                        data: JSON.stringify(sessionData),
                        contentType: 'text/plain',
                        dataType: 'json',
                        async: false,
                        success: function (data) {
                            console.log("Event success.....");
                            debugger;
                            resolve(data);
                        },
                        error: function (error) {
                            console.log(error);
                            console.log("Event failed.....");
                            reject(error);
                        }
                    });

                })

            }

            var _getDeviceDetails = function () {
                'use strict';

                var module = {
                    options: [],
                    header: [navigator.platform, navigator.userAgent, navigator.appVersion, navigator.vendor, window.opera],
                    dataos: [
                        { name: 'Windows Phone', value: 'Windows Phone', version: 'OS' },
                        { name: 'Windows', value: 'Win', version: 'NT' },
                        { name: 'iPhone', value: 'iPhone', version: 'OS' },
                        { name: 'iPad', value: 'iPad', version: 'OS' },
                        { name: 'Kindle', value: 'Silk', version: 'Silk' },
                        { name: 'Android', value: 'Android', version: 'Android' },
                        { name: 'PlayBook', value: 'PlayBook', version: 'OS' },
                        { name: 'BlackBerry', value: 'BlackBerry', version: '/' },
                        { name: 'Macintosh', value: 'Mac', version: 'OS X' },
                        { name: 'Linux', value: 'Linux', version: 'rv' },
                        { name: 'Palm', value: 'Palm', version: 'PalmOS' }
                    ],
                    databrowser: [
                        { name: 'Chrome', value: 'Chrome', version: 'Chrome' },
                        { name: 'Firefox', value: 'Firefox', version: 'Firefox' },
                        { name: 'Safari', value: 'Safari', version: 'Version' },
                        { name: 'Internet Explorer', value: 'MSIE', version: 'MSIE' },
                        { name: 'Opera', value: 'Opera', version: 'Opera' },
                        { name: 'BlackBerry', value: 'CLDC', version: 'CLDC' },
                        { name: 'Mozilla', value: 'Mozilla', version: 'Mozilla' }
                    ],
                    init: function () {
                        var agent = this.header.join(' '),
                            os = this.matchItem(agent, this.dataos),
                            browser = this.matchItem(agent, this.databrowser);

                        return { os: os, browser: browser };
                    },
                    matchItem: function (string, data) {
                        var i = 0,
                            j = 0,
                            html = '',
                            regex,
                            regexv,
                            match,
                            matches,
                            version;

                        for (i = 0; i < data.length; i += 1) {
                            regex = new RegExp(data[i].value, 'i');
                            match = regex.test(string);
                            if (match) {
                                regexv = new RegExp(data[i].version + '[- /:;]([\\d._]+)', 'i');
                                matches = string.match(regexv);
                                version = '';
                                if (matches) { if (matches[1]) { matches = matches[1]; } }
                                if (matches) {
                                    matches = matches.split(/[._]+/);
                                    for (j = 0; j < matches.length; j += 1) {
                                        if (j === 0) {
                                            version += matches[j] + '.';
                                        } else {
                                            version += matches[j];
                                        }
                                    }
                                } else {
                                    version = '0';
                                }
                                return {
                                    name: data[i].name,
                                    version: parseFloat(version)
                                };
                            }
                        }
                        return { name: 'unknown', version: 0 };
                    }
                };

                var e = module.init(),
                    debug = {};

                debug.osName = e.os.name;
                debug.osVersion = e.os.version;
                debug.browserName = e.browser.name;
                debug.browserVersion = e.browser.version;

                debug.userAgent = navigator.userAgent;
                debug.appVersion = navigator.appVersion;
                debug.platform = navigator.platform;
                debug.vendor = navigator.vendor;

                return debug;
                //document.getElementById('log').innerHTML = debug;
            };

            return {

                /*	Method: start
						When this method is invoked, jsReplay will begin to record all user events that occur on the web page.
				*/
                "start": function () {
                    if (playbackInProgress == false) {

                        console.log("Start recording.");

                        // Record the time that occurred from the page being loaded to the recording started. We will
                        // subtract this value from the timestamp on the events in order to eliminate the dead time from our playback JSON log.
                        startTimeDelay = Math.abs(startTimeDelay - new Date().getTime());
                        recordInProgress = true;

                    } else {
                        throw new Error("Cannot start recording -- test playback is in progress.");
                    }
                },

                /*	Method: stop
						When this method is invoked, jsReplay will stop recording user events and save playback JSON to the firebase.
				*/
                "stop": function () {

                    console.log("Stop recording.");

                    recordInProgress = false;

                    var playbackScript = {
                        "window": {
                            "browserDimension": {
                                "width": window.innerWidth,
                                "height": window.innerHeight
                            },
                            "userDeviceDetails": _getDeviceDetails()

                        },
                        "event_log": userEventLog,
                        "HTML": document.documentElement.innerHTML
                    };
                    _saveSessionDetailstoDB(playbackScript).then((data)=>{
                        _saveSessionDatatoDB(playbackScript,data)
                    });

                    _saveSessionDetailstoDB.then((data) =>{
                        console.log(data);
                        console.log("session data saved successfully")
                    } )

                }
            };


        })()

    };
})();

window.onload = function () {
    debugger;
    jsReplay.record.start();
};

window.onbeforeunload = function (e) {
    debugger;
    jsReplay.record.stop();
};