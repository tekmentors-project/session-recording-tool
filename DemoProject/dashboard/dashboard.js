//var USER_DB_PATH = 'https://session-recording-tool.firebaseio.com/users/';
var USER_DB_PATH = 'https://irecord-1104f.firebaseio.com';

// Initialize Firebase
var config = {
    apiKey: "AIzaSyAwoCe0N8EhgDDdkBTHpl6D9LM5cjFMWic",
    authDomain: "session-recording-tool.firebaseapp.com",
    databaseURL: "https://session-recording-tool.firebaseio.com",
    projectId: "session-recording-tool",
    storageBucket: "session-recording-tool.appspot.com",
    messagingSenderId: "571506989664"
};
firebase.initializeApp(config);

firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
        //getUserWebsites(user.uid);
        getUserWebsites();
    }
    else {
        window.location.href = './index.html';
    }
});

//function getUserWebsites(userName){
function getUserWebsites(){
    $.ajax({
        //url: USER_DB_PATH + userName +'.json',
        url: USER_DB_PATH +'/website.json?shallow=true',
        type: 'GET',
        contentType: 'text/plain',
        dataType: 'json',
        success: function(websiteDetails) {
            renderWebsiteDetails(websiteDetails);
        },
        error: function(error) {
            console.log(error);
            $('#loaderWeb').hide();
        }
    });
}

function renderWebsiteDetails(websiteDetails){
    if(websiteDetails){
        $('#loaderWeb').hide();
        /*Object.keys(websiteDetails).forEach(eachWebsite => {
            var eachWebsiteDetails=websiteDetails[eachWebsite];
            var htmlContent='<a href="../playback/playback.html?websiteKeyName='+eachWebsiteDetails.websiteKey+'" target="_blank"><div class="card"><div class="card-body text-center"><h4 class="card-title">'+eachWebsiteDetails.websiteName+'</h4><p class="card-ext">Membership:: '+eachWebsiteDetails.websiteValidity+'</p></div></div></a>';

            $('#addTableContent').append(htmlContent);
        });*/
        Object.keys(websiteDetails).forEach(eachWebsite => {
            var htmlContent='<a href="../playback/playback.html?websiteKeyName='+eachWebsite+'" target="_blank"><div class="card mb-4"><div class="card-body text-center"><h4 class="card-title">'+eachWebsite+'</h4><p class="card-ext">Membership:: 23-10-2019</p></div></div></a>';

            $('#addTableContent').append(htmlContent);
        });
    }else{
        console.log('No details exists for this user');
        var htmlContent='<div class="card"><div class="card-body text-center"><h4 class="card-title">Please register your website by adding the above script to your website.</h4><p class="card-ext">Currently there are no registered websites for your account.</p></div></div>';

        /*<img class="img-fluid" src="'+eachWebsiteDetails.websiteImageURL+'">*/
        $('#loaderWeb').hide();
        $('#addTableContent').append(htmlContent);
    }
}


function copyRecordJS() {
    var copyText = document.getElementById("textAreaRecordJS");
    copyText.select();
    document.execCommand("copy");

    document.getElementById("recordJSTooltip").innerHTML = "Copied to clipboard!";
}

function outFunc() {
    document.getElementById("recordJSTooltip").innerHTML  = "Copy to clipboard !";
}


////////////

function logoutUser() {
    firebase.auth().signOut().then(function() {
        window.location.href = './index.html';
    }).catch(function(error) {
        console.log(error);
    });
}
