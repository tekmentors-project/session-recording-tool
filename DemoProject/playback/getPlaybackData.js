
(function () {

    let BASE_PATH = "https://irecord-1104f.firebaseio.com/website/";

    function _getUrlParameter(name) {
        name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
        var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
        var results = regex.exec(location.search);
        return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
    };

    let websiteName = _getUrlParameter("websiteKeyName");
    // TODO need to remove this
    if (!websiteName) {
        websiteName = 'google_co_in'
    }

    let widgetTest;

    window.onload = function () {
        let getDatesUrl = BASE_PATH + websiteName
        let testArr = [];
        let series = Promise.resolve();
        getDates(getDatesUrl)
            .then((allDates) => {
                let datesArr = Object.keys(allDates);
                datesArr.forEach((date) => {
                    series = series.then(() => {
                        renderDate(date).then((_result) => {
                            return new Promise((resolve) => {
                                testArr.push({
                                    "date": date,
                                    "sessions": _result
                                })
                                if (testArr.length === datesArr.length) {
                                    resolve(testArr);
                                }
                            }).then(() => {
                                debugger;
                                console.log("after series then", testArr);
                                _createSessionCards(testArr);
                            }).then(() => {
                                _addGetSessionDataOnClickHandler();
                                $('#loaderWeb').hide();
                                $('#secondary').show();
                              })
                        })

                    })

                })
            })
    }

    function getDates(getDatesUrl) {

        //debugger;
        return new Promise((resolve, reject) => {
            $.ajax({
                url: getDatesUrl + '.json?shallow=true',
                type: 'GET',
                contentType: 'text/plain',
                dataType: 'json',
                success: function (allDates) {
                    //traverseDates(getDates);
                    //renderDates(getDates);
                    resolve(allDates);
                },
                error: function (error) {
                    console.log(error)
                }
            });
        });
    }

    function renderDate(date) {
        let sessions = [];
        return getSessionKeys(date)
            .then((sessionKeys) => {
                Object.keys(sessionKeys).forEach((sessionKey) => {
                    sessions.push(getSessionDetails(date, sessionKey))
                })
                return Promise.all(sessions);
            });
    }

    function getSessionKeys(date) {

        let getSessionKeysUrl = BASE_PATH + websiteName + '/' + date + '.json?shallow=true';
        //debugger;
        return new Promise((resolve, reject) => {
            $.ajax({
                url: getSessionKeysUrl,
                type: 'GET',
                contentType: 'text/plain',
                dataType: 'json',
                success: function (sessionKeys) {
                    resolve(sessionKeys);
                },
                error: function (error) {
                    reject(error);
                }
            });
        })
    }

    function getSessionDetails(eachDate, sessionKey) {

        let getSessionDetailsUrl = BASE_PATH + websiteName + '/' + eachDate + '/' + sessionKey + '/' + 'sessionDetails.json';
        //debugger;
        return new Promise((resolve, reject) => {
            $.ajax({
                url: getSessionDetailsUrl,
                type: 'GET',
                contentType: 'text/plain',
                dataType: 'json',
                success: function (sessionDetail) {
                    sessionDetail['date'] = eachDate;
                    sessionDetail['sessionId'] = sessionKey;
                    resolve(sessionDetail);
                },
                error: function (error) {
                    console.log(error)
                }
            });
        });
    }

    function getSessionData(eachDate, sessionKey) {

        let getSessionDataUrl = BASE_PATH + websiteName + '/' + eachDate + '/' + sessionKey + '/' + 'sessionData.json';
        //debugger;
        return new Promise((resolve, reject) => {
            $.ajax({
                url: getSessionDataUrl,
                type: 'GET',
                contentType: 'text/plain',
                dataType: 'json',
                success: function (sessionData) {
                    resolve(sessionData);
                },
                error: function (error) {
                    console.log(error)
                }
            });
        });
    }

    var _createSessionCards = function (datewiseSessions) {

        datewiseSessions = _sortSessions(datewiseSessions);


        let sessionCardsElements = datewiseSessions.map((element, index) => {
            let sessionCards = "";

            sessionCards = element.sessions.map((ele) => {
                if (ele.baseUrl.indexOf('file') > -1) {
                    ele.baseUrl = 'google.co.in';
                }

                return '<div class="row sessionRow" id="'
                    + ele.sessionId
                    + '" name="'
                    + ele.date
                    + '"><div class="col-2 col-lg-1 text-center m-auto"><img class="img-thumbnail mx-auto" src="./vedio.png" alt="Session Image"></div><div class="col-10 col-lg-11"><h5 class="card-title">'
                    + ele.date
                    + '</h5><p class="card-text"> Website : '
                    + ele.baseUrl
                    + '</p><a href="#" class="btn btn-success" style="border-radius: 10rem;">'
                    + ele.sessionLength
                    + '</a></div></div><hr>';

            })


            return '<div class="card"><div class="card-header" id="heading"><h5 class="mb-0"><button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse_'
                + index
                + '" aria-expanded="true" aria-controls="collapseOne"><span>'
                + element.date
                + '</button></h5></div><div id="collapse_'
                + index
                + '" class="collapse" aria-labelledby="headingOne" data-parent="#sessionsListContainer"><div class="card-body p-1"><div id="sessionsList" class="col-12 p-0">'
                + sessionCards
                + '</div></div></div></div>';
        })

        $('#sessionsListContainer').append(sessionCardsElements);
    }

    function _addGetSessionDataOnClickHandler() {
        $('.sessionRow').click(function (event) {
            //debugger;
            event.stopPropagation();
            event.preventDefault();
            let clickedSessionId = event.currentTarget.getAttribute('id');
            let clickedSessionDate = event.currentTarget.getAttribute('name');
            /* console.log(event.currentTarget.getAttribute('id'));

            debugger; */

            getSessionData(clickedSessionDate,clickedSessionId).then((sessionData)=>{
                storedataInLocalStorage(sessionData);
                //openWindow(clickedSessionDate,clickedSessionId,sessionData);
                openWindow();
            })/* .then(()=>{
                openWindow();
            }) */

            
          /*   let getSessionDataUrl = BASE_PATH+websiteName + '/' + clickedSessionDate + '/' + clickedSessionId + '/' + 'sessionData';
            
            widgetTest = new jsReplay.playback(getSessionDataUrl); */

        })
    }

    function _sortSessions(datewiseSessions){

        console.log(datewiseSessions);

        datewiseSessions.sort(function(a,b){
            
            var a_dateParts = a.date.split("_");
            var b_dateParts = b.date.split("_");

            var a_dateObject = new Date(a_dateParts[2], a_dateParts[1] - 1, a_dateParts[0]);
            var b_dateObject = new Date(b_dateParts[2], b_dateParts[1] - 1, b_dateParts[0]);

            // Turn your strings into dates, and then subtract them
            // to get a value that is either negative, positive, or zero.
            return b_dateObject - a_dateObject;
          });

          console.log(datewiseSessions);

          return datewiseSessions;

    }

    function storedataInLocalStorage(sessionData){
        localStorage.setItem('playbackScriptData',JSON.stringify(sessionData));
    }


    function openWindow(){
        let playbackScriptData = localStorage.getItem('playbackScriptData');

        playbackScriptData = JSON.parse(playbackScriptData);

        var playBackSessionWindow = window.open("", "playBackSessionWindow", "width=" + playbackScriptData.window.browserDimension.width + ",height=" + playbackScriptData.window.browserDimension.height);

        playBackSessionWindow.document.write(playbackScriptData.HTML);
        playBackSessionWindow.document.write('<script src="./playback3.js"></script>');
        playBackSessionWindow.document.write(
            "<div id='sessionRecorderOverlay' style='z-index: 2147483690;position: fixed;top: 0;left: 0;height: 100vh;width: 100vw;background: rgba(0,0,0,0.6);'><button style='position: absolute;top: 45%;left: 45%;border-style: solid;box-sizing: border-box;border-width: 2.5rem 0 2.5rem 5rem;border-color: transparent transparent transparent #202020;background-color: transparent;cursor: pointer; z-index: 2147483700;' onclick='playRecording()'></button></div>"
        );
        
        /* playBackSessionWindow.document.write(`<script>
            function playRecording() {
                document.getElementById("sessionRecorderOverlay").style.display = "none"
                var widgetTest = new jsReplay.playback("");
                widgetTest.start();
            }
            </script>`
        ); */

    }

    /* function openWindow(clickedSessionDate,clickedSessionId,sessionData){
        let playbackScriptData = localStorage.getItem('playbackScriptData');

        playbackScriptData = JSON.parse(playbackScriptData);    
        //var playBackSessionWindow = window.open("?clickedSessionDate="+clickedSessionDate+"&clickedSessionId="+clickedSessionId, "playBackSessionWindow", "width=" + playbackScriptData.window.browserDimension.width + ",height=" + playbackScriptData.window.browserDimension.height);

        var playBackSessionWindow = window.open('"?clickedSessionDate="+clickedSessionDate+"&clickedSessionId="+clickedSessionId', "playBackSessionWindow", "width=" + playbackScriptData.window.browserDimension.width + ",height=" + playbackScriptData.window.browserDimension.height);
        playBackSessionWindow.document.write(playbackScriptData.HTML);
        playBackSessionWindow.document.write(
            "<div id='sessionRecorderOverlay' style='position: fixed;top: 0;left: 0;height: 100vh;width: 100vw;background: rgba(0,0,0,0.6);'><button style='position: absolute;top: 45%;left: 45%;border-style: solid;box-sizing: border-box;border-width: 2.5rem 0 2.5rem 5rem;border-color: transparent transparent transparent #202020;background-color: transparent;cursor: pointer;' onclick='playRecording()'></button></div>"
        )
        playBackSessionWindow.document.write(`<script>
            function playRecording() {
                document.getElementById("sessionRecorderOverlay").style.display = "none"
                var widgetTest = new jsReplay.playback("");
                widgetTest.start();
            }
            </script>`
        );
        playBackSessionWindow.document.write(`<script>
        function _getUrlParameter(name) {
            name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
            var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
            var results = regex.exec(location.search);
            return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
        };

        let clickedSessionDate = _getUrlParameter("clickedSessionDate");
        let clickedSessionId = _getUrlParameter("clickedSessionId");
        debugger;
        </script>`)
    } */

    function playRecording() {
        document.getElementById("sessionRecorderOverlay").style.display = "none"
        var widgetTest = new jsReplay.playback("");
        widgetTest.start();
    }


 /*    var playSession = function(){
        try{
            if(widgetTest && widgetTest instanceof jsReplay.playback){
                widgetTest.start();
            }else{
                alert("please select a session to play");
            }
            
        }catch(e){
            if(e instanceof ReferenceError){
                alert("please select a session to play");
            }
        }
     }
    jQuery("#playSessionButton").on('click', playSession); */
})()