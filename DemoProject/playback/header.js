// Initialize Firebase
var config = {
    apiKey: "AIzaSyAwoCe0N8EhgDDdkBTHpl6D9LM5cjFMWic",
    authDomain: "session-recording-tool.firebaseapp.com",
    databaseURL: "https://session-recording-tool.firebaseio.com",
    projectId: "session-recording-tool",
    storageBucket: "session-recording-tool.appspot.com",
    messagingSenderId: "571506989664"
};
firebase.initializeApp(config);
firebase.auth().onAuthStateChanged(function(user) {
    if (!user) {
        window.location.href = '../dashboard/index.html';
    }
});
function logoutUser() {
    firebase.auth().signOut().then(function() {
        window.location.href = '../dashboard/index.html';
    }).catch(function(error) {
        console.log(error);
    });
}