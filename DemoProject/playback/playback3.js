//var widgetTest = {};
var jsReplay = (function () {

    // Indicates whether or not jsReplay is playing back user events. When set to true, jsReplay will not start another playback nor record user events.
    var playbackInProgress = false;

    // Indicates whether or not jsReplay is recording user events. When set to true, jsReplay will not start another recording nor start a playback.
    var recordInProgress = false;

    return {

        "playback": (function () {

            var selectorHash = {};

            /* 	Function: verifyContains 
					Verifies whether the element specified by the userEvent.selector contains the text stored in userEvent.text

				Parameters:
					userEvent - Object, a single DOM event from the JSON playback file. 

				Returns:
					Boolean - true if the element does contain the specified text or false if it does not.
			*/
            var verifyContains = function (userEvent) {

                var elementText = $(userEvent.selector).val() || $(userEvent.selector)[0].innerHTML;

                if (elementText.indexOf(userEvent.text) !== -1) {
                    console.log("PASS - element does contain specified text.");
                } else {
                    throw new Error("FAIL - element does not contain specified text.");
                }
            };

            /*	Function: simulateEvent
					Replays the DOM event specified by userEvent -- uses the same event type and same coordinates that were originally recorded for the event.

				Parameters:
					userEvent - Object, a single DOM event from the JSON playback file. 

				Returns:
					Nothing.				
			*/
            var simulateEvent = function (userEvent) {

                if (userEvent.selector in selectorHash) {
                    var eventTarget = selectorHash[userEvent.selector];
                } else {

                    if (userEvent.selector === "document") {
                        var eventTarget = document;
                    } else {
                        var eventTarget = $(userEvent.selector)[0];
                    }

                    if (userEvent.hasOwnProperty("clientX") && userEvent.hasOwnProperty("clientY")) {

                        // get the target based on the click coordinates
                        var target = document.elementFromPoint(userEvent.clientX, userEvent.clientY);

                        // verify that the target from the coordinates matches the logged CSS selector
                        if (target === eventTarget) {
                            console.log("PASS - click target matches selector element.");
                            selectorHash[userEvent.selector] = eventTarget;
                        } else {
                            throw new Error("FAIL - Element at point (" + userEvent.clientX + "px, " + userEvent.clientY + "px) does not match selector " + userEvent.selector);
                        }

                    }
                }

                console.log("Simulating scroll (" + (userEvent.timeStamp / 1000).toFixed(3) + "s). Selector: " + userEvent.selector);

                var event = null;

                switch (userEvent.type) {
                    case "scroll":
                        $(eventTarget).scrollLeft(userEvent.scrollLeft);
                        $(eventTarget).scrollTop(userEvent.scrollTop);
                        break;
                    case "focusin":
                    case "focusout":
                    case "focus":
                    case "blur":
                        event = new FocusEvent(userEvent.type, userEvent);
                        break;
                    case "tap":
                    case "click":
                    case "mouseup":
                    case "mousedown":
                        event = new MouseEvent(userEvent.type, userEvent);
                        break;
                    case "touchstart":
                    case "touchend":
                    case "touchmove":
                    case "touchcancel":

                        var touchList = [];
                        for (var i = 0; i < userEvent.touches.length; i++) {
                            var touch = userEvent.touches[i];
                            var newTouch = new Touch({
                                "clientX": touch.clientX,
                                "clientY": touch.clientY,
                                "force": touch.force,
                                "identifier": touch.identifier,
                                "pageX": touch.pageX,
                                "pageY": touch.pageY,
                                "radiusX": touch.radiusX,
                                "radiusY": touch.radiusY,
                                "rotationAngle": touch.rotationAngle,
                                "screenX": touch.screenX,
                                "screenY": touch.screenY,
                                "target": $(touch.selector)[0]
                            });
                            touchList.push(newTouch);
                        }

                        userEvent.touches = touchList;

                        var touchList = [];
                        for (var i = 0; i < userEvent.changedTouches.length; i++) {
                            var touch = userEvent.changedTouches[i];
                            var newTouch = new Touch({
                                "clientX": touch.clientX,
                                "clientY": touch.clientY,
                                "force": touch.force,
                                "identifier": touch.identifier,
                                "pageX": touch.pageX,
                                "pageY": touch.pageY,
                                "radiusX": touch.radiusX,
                                "radiusY": touch.radiusY,
                                "rotationAngle": touch.rotationAngle,
                                "screenX": touch.screenX,
                                "screenY": touch.screenY,
                                "target": $(touch.selector)[0]
                            });
                            touchList.push(newTouch);
                        }

                        userEvent.changedTouches = touchList;

                        event = new TouchEvent(userEvent.type, userEvent);

                        break;
                    case "keypress":
                    case "keydown":
                    case "keyup":
                        event = new KeyboardEvent(userEvent.type, userEvent);
                        break;
                    case "input":
                        event = new Event(userEvent.type, userEvent);
                        $(userEvent.selector).val(userEvent.value);
                        break;
                    case "contains":
                        verifyContains(userEvent);
                        return;
                    default:
                        throw new Error("Unsupported event type.");
                        break;
                }

                if (event !== null) {
                    eventTarget.dispatchEvent(event);
                }

            };


            /*	Playback constructor function. Unlike recording, to playback a test the user must 
				create a new instance of the playback constructor and manually start it.

				Parameters:
					testRunURL - String, the URL where the JSON playback file is stored.
			*/
            //var testRunURL = 'https://session-recording-tool.firebaseio.com/username.json';
            var constructor = function (testRunURL) {
                debugger;
                var self = this;

                /*	this.window
						Object, stores the width and height attributes that the playback JSON file was designed to run in. It is essential
						that the playback occur in a web browser window with the same dimensions as the original test run recording.
				*/
                this.window = null;

                /*	Property: this.userEventLog
						Array of events, this is where the recorded events are stored. Each event contains most standard event properties as well as 
						some additional properties (selector and text) used for identifying the element and the contents of the element. The events are ordered
						oldest to newest (i.e., the events that were recorded first are at the beginning of the array).
				*/
                this.userEventLog = null;

                this.handleSuccess = function (playbackData) {
                    // Validate the playback file we've received
                    if (typeof playbackData == "object") {

                        // We won't run the playback file without the window attributes (i.e., browser window dimensions)
                        if (typeof playbackData.window == "object") {
                            self.window = playbackData.window;
                        } else {
                            throw new Error("Playback JSON file does not contain required window attributes.");
                        }

                        // Verify that the event_log is an array, if it's not an array, then this is an invalid playback JSON file.
                        if (Array.isArray(playbackData.event_log)) {
                            self.userEventLog = playbackData.event_log;
                        } else {
                            throw new Error("Event log in the JSON playback file is not an array.");
                        }
                    } else if (typeof playbackData == "string") {
                        playbackData = JSON.parse(playbackData);
                        // We won't run the playback file without the window attributes (i.e., browser window dimensions)
                        if (typeof playbackData.window == "object") {
                            self.window = playbackData.window;
                        } else {
                            throw new Error("Playback JSON file does not contain required window attributes.");
                        }

                        // Verify that the event_log is an array, if it's not an array, then this is an invalid playback JSON file.
                        if (Array.isArray(playbackData.event_log)) {
                            self.userEventLog = playbackData.event_log;
                        } else {
                            throw new Error("Event log in the JSON playback file is not an array.");
                        }
                    } else {
                        throw new Error("Received an invalid playback JSON file.");
                    }
                }
                debugger;
                if (!testRunURL) {
                    let playbackData = localStorage.getItem('playbackScriptData');
                    console.log("start playing data");
                    console.log(playbackData);
                    this.handleSuccess(playbackData);
                } else {
                    console.log("In else with testRunURL:: " + testRunURL);
                    $.ajax({
                        url: testRunURL,
                        success: function (playbackData) {

                            console.log("PlaybackData::" + playbackData);
                            this.handleSuccess(playbackData);
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            throw new Error("Failed to retrieve the playback JSON file.");
                        },
                        dataType: "json"
                    });
                }
            };

            constructor.prototype = {

                /*	Method: start
						This method will start the playback of the user event log.
				*/
                "start": function () {

                    debugger;

                    var self = this;

                    if (playbackInProgress !== false) {
                        throw new Error("Cannot start playback -- there is another test playback already in-progress.");
                        return;
                    }

                    if (recordInProgress !== false) {
                        throw new Error("Cannot start playback -- a recording is already in-progress.");
                        return;
                    }

                    if (window.innerHeight !== this.window.browserDimension.height || window.innerWidth !== this.window.browserDimension.width) {
                        throw new Error("Cannot start playback -- browser window must match dimensions that the playback script was recorded in (" + this.window.width + "px by " + this.window.height + "px). Window is currently " + window.innerWidth + "px by " + window.innerHeight + "px.");
                        return;
                    }

                    console.log("Starting test script playback.");

                    playbackInProgress = true;

                    // record the time that the user started the playback
                    var timeStartedPlayback = new Date().getTime();

                    // run the setInterval on a very short 10ms iteration so we can, as closely as possible, siumulate events exactly when they 
                    // were originally fired
                    var runSimulator = setInterval(function () {

                        var currentTime = new Date().getTime();

                        // we store the array length as a variable for performance reasons (faster than continually accessing the .length property).
                        var userEventLength = self.userEventLog.length;

                        // if the current time is greater than the timestamp of the first event in the array (e.g., 3000ms) plus when the playback started, 
                        // then the event should be triggered
                        if (self.userEventLog && self.userEventLog.length >= 0 && (currentTime > (self.userEventLog[0].timeStamp + timeStartedPlayback))) {
                            do {
                                // we're going to trigger this event, so we remove it from the array
                                var userEvent = self.userEventLog.splice(0, 1)[0];

                                // reduce the array length, must be done manually since we've stored the length in a variable for performance reasons
                                userEventLength--;

                                // trigger the event
                                simulateEvent(userEvent);

                                // continue this loop for events that occurred up to 50ms in the future. we do this because a simple user action like a mouse click
                                // will trigger multiple events (click, mousedown, mouseup, etc). if those events were separated by even 10ms, then the DOM could change in-between
                                // those events and we'd encounter an element target mismatch. looking forward 200ms and firing them at the same time allows us to avoid this issue.
                            } while (userEventLength > 0 && ((currentTime + 50) > (self.userEventLog[0].timeStamp + timeStartedPlayback)));
                        }

                        // if userEventLength is 0, then that means there are no more events to replay
                        if (userEventLength == 0) {
                            clearInterval(runSimulator);
                            console.log("Test script playback finished.");
                            playbackInProgress = false;
                        }
                    }, 10);

                }

            }

            return constructor;

        })()
    };
})();

/* window.onload = function () {
    debugger;
    widgetTest = new jsReplay.playback("");
}; */

function playRecording() {
    debugger;
    document.getElementById("sessionRecorderOverlay").style.display = "none";
    let inputElementsArr = document.getElementsByTagName('input');
    for(let i = 0; i < inputElementsArr.length ; i++){
        if(inputElementsArr[i].type == 'radio' || inputElementsArr[i].type == 'reset' || inputElementsArr[i].type == 'submit' ){
            //inputElementsArr[i].type = "radio"; do nothing 
        }else{
            inputElementsArr[i].type = "password";
        }
    }
    var widgetTest = new jsReplay.playback("");
    widgetTest.start();
}